const mongoose = require('mongoose');
const config = require('./config');

const Product = require('./models/Product');
const Category = require('./models/Category');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

const collections = ['categories', 'products'];

db.once('open', async () => {
    collections.forEach(async collectionName => {
        try {
            await db.dropCollection(collectionName);
        } catch (e) {
            console.log(`Collection ${collectionName} did not exist in DB`);
        }
    });

    const [compsCategory, carsCategory, instrumentsCategory] = await Category.create(
        {
            title: 'Computers',
        },
        {
            title: 'Cars',
        },
        {
            title: 'Instruments'
        }
    );

    const [notebook, accord, guitar] = await Product.create(
        {
            title: 'ASUS Notebook',
            price: 50000,
            description: 'Very cool notebook',
            category: compsCategory._id,
            image: 'notebook.jpg'
        },
        {
            title: 'Honda Accord',
            price: 210000,
            description: 'Year 2017',
            category: carsCategory._id,
            image: 'accord.jpg'
        },
        {
            title: 'Guitar',
            price: 4000,
            description: 'Made by Yamaha',
            category: instrumentsCategory._id,
            image: 'guitar.jpg'
        }
    );

    db.close();
});