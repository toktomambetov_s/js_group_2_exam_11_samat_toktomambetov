const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const ObjectId = require('mongodb').ObjectId;

const Product = require('../models/Product');

const config = require('../config');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = db => {

    router.get('/', (req, res) => {
        Product.find().populate('category')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', upload.single('image'), (req, res) => {
        const productData = req.body;

        if (req.file) {
            productData.image = req.file.filename;
        } else {
            productData.image = null;
        }

        const product = new Product(productData);

        product.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {
        db.collection('products')
            .findOne({_id: new ObjectId(req.params.id)})
            .then(result => {
                if (result) res.send(result);
                else res.status(404).send({error: 'Not found'});
            })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;