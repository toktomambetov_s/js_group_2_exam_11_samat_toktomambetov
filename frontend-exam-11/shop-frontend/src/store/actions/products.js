import axios from '../../axios-api';
import {FETCH_PRODUCTS_SUCCESS} from "./actionTypes";

export const fetchProductsSuccess = products => {
    return {type: FETCH_PRODUCTS_SUCCESS, products}
};

export const fetchProducts = () => {
    return dispatch => {
        axios.get('/products').then(
            response => dispatch(fetchProductsSuccess(response.data))
        );
    }
};