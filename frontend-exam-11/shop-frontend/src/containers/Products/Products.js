import React, { Component, Fragment } from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ProductListItem from "../../components/ProductListItem/ProductListItem";
import {fetchProducts} from "../../store/actions/products";

class Products extends Component {
    componentDidMount() {
        this.props.onFetchProducts();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    All Items
                </PageHeader>

                {this.props.products.map(product => (
                    <ProductListItem
                        key={product._id}
                        id={product._id}
                        title={product.title}
                        price={product.price}
                        image={product.image}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.products.products
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProducts: () => dispatch(fetchProducts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);