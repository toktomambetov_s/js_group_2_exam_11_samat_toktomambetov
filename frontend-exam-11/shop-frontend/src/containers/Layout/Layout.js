import React, { Fragment } from 'react';

import Toolbar from "../../components/UI/Toolbar/Toolbar";

const Layout = props => (
    <Fragment>
        <header>
            <Toolbar user={props.user} logout={props.logoutUser}/>
        </header>
        <main className="container">
            {props.children}
        </main>
    </Fragment>
);

export default Layout;