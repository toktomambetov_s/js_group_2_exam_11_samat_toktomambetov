import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

import AnonymousMenu from "./Menus/Anonymous";

const Toolbar = ({user, logout}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>Flea market</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>
                <LinkContainer to="/" exact>
                    <NavItem>All Items</NavItem>
                </LinkContainer>
            </Nav>

            <AnonymousMenu/>
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;